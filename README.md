Diese Repo legt die Analyse hinter dem Artikel «Wie klimabewusst legen Schweizer Banken an?» teilweise offen.

Folgen Sie [diesem Link](https://balthasars.gitlab.io/fossile-investitionen-geschaeftsbanken-serve/index.html) für die visualisierte Form der Daten.

**Struktur**

- `scr`
    - `00_prepare_cusips_for_eikon.R`
        - portioniert CUSIP-Wertschriften-Identifiers in Excel-freundliche Portionen
    - `01_prep_eikon_meta.R`
        - bereitet die Metadaten von Refinitiv auf
    - `02_get_openfigi_meta_for_filings.R`
        - Dowload weiterer Metadaten aus OpenFIGI 
    - `helpers.R`
        - Helfer-Funktionen, die projektübergreifend verwendet werden.
- Weitere Datenaufbereitung und -analyse
    - `00-banken-identifizieren.Rmd`
        - CIK der Banken identifizeren
    - `01-filings-beziehen.Rmd`
        - Filings herunterladen
    - `02-filings-first-look-ts.Rmd`
        - Pre-Processing der Filings
        - erste Due Diligence 
    - `03-filings-mit-meta-ts.Rmd`
        - Berechnungen
        - Analyse
        - Erklärungen
- `raw`
    - enthält Rohdaten
- `data` 
    - enthält aufbereitete Daten