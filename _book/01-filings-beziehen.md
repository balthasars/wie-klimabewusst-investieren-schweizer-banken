---
output:
  html_document:
     df_print: paged
  pdf_document: default
editor_options:
  chunk_output_type: inline
site: bookdown::bookdown_site
---



# Bezug 13F-Filings


```r
library(dplyr)
library(tidysec)
library(ggplot2)
theme_set(theme_minimal())
library(tidyverse)
knitr::clean_cache()
```

```
## NULL
```

Central Index Keys der zu analysierenden Geschäftsbanken einlesen:


```r
banken_nach_cik <- vroom::vroom(here::here("raw", "ciks_banks.csv"), delim = "*")
ciks_vec <- banken_nach_cik %>% pull(cik)
```

Dann die 13F-Filings mit dem [`{tidysec}` Package](https://github.com/balthasars/tidysec) herunterladen:


```r
alle_raw <- get_13f(cik = ciks_vec, year = 2021, amendments = TRUE)
```

```
## [1] "No filings found for CIK 1068833,1321482,903954,1114446,1528147,1615424,1615305,1689918,1658354,1667654,1649591,1650162,1649592,1536550"
## [1] "No filings found for CIK 1068833,1321482,903954,1114446,1528147,1615424,1615305,1689918,1658354,1667654,1649591,1650162,1649592,1536550"
```

```r
alle_raw
```

```
## # A tibble: 30 x 28
##    cik    company_name   form_type date_filed quarter link_to_filing     filing 
##    <chr>  <chr>          <chr>          <dbl>   <dbl> <chr>              <list> 
##  1 11327… UBS OCONNOR L… 13F-HR/A        2021       1 https://www.sec.g… <tibbl…
##  2 11327… UBS OCONNOR L… 13F-HR/A        2021       1 https://www.sec.g… <tibbl…
##  3 11327… UBS OCONNOR L… 13F-HR/A        2021       1 https://www.sec.g… <tibbl…
##  4 11327… UBS OCONNOR L… 13F-HR/A        2021       1 https://www.sec.g… <tibbl…
##  5 11327… UBS OCONNOR L… 13F-HR          2021       1 https://www.sec.g… <tibbl…
##  6 12912… PICTET ASSET … 13F-HR          2021       1 https://www.sec.g… <tibbl…
##  7 13681… Zurcher Kanto… 13F-HR          2021       1 https://www.sec.g… <tibbl…
##  8 14917… Lombard Odier… 13F-HR          2021       1 https://www.sec.g… <tibbl…
##  9 15356… BANQUE PICTET… 13F-HR          2021       1 https://www.sec.g… <tibbl…
## 10 15356… PICTET BANK &… 13F-HR          2021       1 https://www.sec.g… <tibbl…
## # … with 20 more rows, and 21 more variables: filing_number <chr>,
## #   submissionType <chr>, filingManager <chr>, reportType <chr>,
## #   periodOfReport <chr>, reportCalendarOrQuarter <chr>,
## #   form13FFileNumber <chr>, stateOrCountry <chr>, signatureDate <chr>,
## #   tableEntryTotal <chr>, tableValueTotal <chr>,
## #   otherIncludedManagersCount <chr>, coverPage <chr>, isAmendment <chr>,
## #   amendmentNo <chr>, amendmentType <chr>, link_to_primary_doc <chr>,
## #   other_included_managers_name <list>, otherManagerForm13FFileNumber <list>,
## #   other_managers_reporting_for_this_manager_file_number <list>,
## #   other_managers_reporting_for_this_manager <list>
```

In den Filings sind einige uninteressante Kandidaten — wir schauen uns nur Q4/2020 an, deshalb rausfiltern, Offenlegungsdaten selbst unnesten (noch in Listenspalte) und als `.rds` abspeichern.


```r
alle_raw %>% 
   filter(periodOfReport == "12-31-2020") %>% 
   # unnesten des Filings
   unnest(filing) %>%
   # schönere Spaltennamen
   janitor::clean_names() %>% 
   # speichern
   readr::write_rds(file = here::here("raw", "13F", "13f_filings_q4_2020.rds"))
```
