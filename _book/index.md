---
title: "Klimaverträglichkeit der US-Investitionen Schweizer Grossbanken"
date: "2021-04-06"
author: "Balthasar Sager"
site: bookdown::bookdown_site
output:
  bookdown::gitbook:
    df_print: paged
documentclass: book
biblio-style: apalike
link-citations: yes
gitlab-repo: balthasars/fossile-investitionen-geschaeftsbanken
description: "Diese Webseite dient dem einfacheren Teilen der Projektzwischenschritte sowie zur Dokumentation der Datenaufbereitu"
---

# Inhalt

Diese Webseite dient dem einfacheren Teilen der Projektzwischenschritte und der Offenlegung der Analyse (exklusive Carbon Delta-Daten).
