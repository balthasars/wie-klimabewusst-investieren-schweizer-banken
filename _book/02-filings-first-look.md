---
site: bookdown::bookdown_site
output:
  html_document: default
  pdf_document: default
---



# Überblick über die 13F-Filings




Einlesen der Filings und Details zu den Banken:



## Amendments und Restatements

Gewisse Banken legen ihre Positionen nicht sofort vollumfänglich offen, z.T. um strategisch aufgebaute Positionen (noch) nicht offen legen zu müssen.
Sie legen diese dann zu einem späteren mit einem 13F-HR/A (Amendment)-Filing offen, entweder als Restatement (Portfolio wird nochmals komplett offengelegt) oder als Amendment (nur die fehlenden Positionen werden zusätzlich in einem Filing offengelegt).

Um die Portfolios also nicht unter Umständen doppelt zu analysieren, konsolidiere ich die Portfolios:


```
## # A tibble: 24 x 12
##    cik    company_name           filing_number  submission_type table_value_tot…
##    <chr>  <chr>                  <chr>          <chr>                      <dbl>
##  1 11327… UBS OCONNOR LLC        0001132716210… 13F-HR/A                    3903
##  2 11327… UBS OCONNOR LLC        0001132716210… 13F-HR                   3244733
##  3 12912… PICTET ASSET MANAGEME… 0000950123210… 13F-HR                  76622225
##  4 13681… Zurcher Kantonalbank … 0000947871210… 13F-HR                  16421002
##  5 14917… Lombard Odier Asset M… 0001214659210… 13F-HR                   2084302
##  6 15356… BANQUE PICTET & CIE SA 0001535602210… 13F-HR                   7356936
##  7 15356… PICTET BANK & TRUST L… 0001535631210… 13F-HR                    159668
##  8 15356… Lombard Odier Asset M… 0001214659210… 13F-HR                    886957
##  9 15357… Lombard Odier Asset M… 0001214659210… 13F-HR                   1656650
## 10 15445… Bank Julius Baer & Co… 0001544599210… 13F-HR/A                12182188
## # … with 14 more rows, and 7 more variables: is_amendment <chr>,
## #   amendment_no <chr>, amendment_type <chr>, data <list>, bank_name <chr>,
## #   comment <chr>, Comment_2 <chr>
```
- wenn amendment_type == RESTATEMENT, dann andere Filings rausschmeissen
- wenn amendment_type NA oder NEW HOLDINGS ist, dann drinlassen

Wie gross sind die offengelegten Portfolios zusammen in Milliarden?


```
## # A tibble: 1 x 1
##   value_tot
##       <dbl>
## 1      883.
```


Portfolio nach Bank:

<img src="02-filings-first-look_files/figure-html/unnamed-chunk-5-1.png" width="672" />

Portfolios nach Filer:

<img src="02-filings-first-look_files/figure-html/unnamed-chunk-6-1.png" width="672" />

## Checks, ob Beträge überall in Tausenden angegeben

Stimmen die Summen im Portfolio überein mit jenen, die im `primary_doc` angegeben werden?


```{=html}
<div id="htmlwidget-0442ef11d7f8abb3a398" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-0442ef11d7f8abb3a398">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23"],["861177","1132716","1610520","824468","1291274","928633","1368163","1588340","1544599","1615423","1535602","1132716","1649647","1641992","1491719","1793755","1535784","1535660","1599576","1599469","1727588","1634222","1535631"],["UBS ASSET MANAGEMENT AMERICAS INC","UBS OCONNOR LLC","UBS Group AG","CREDIT SUISSE AG/","PICTET ASSET MANAGEMENT LTD","VONTOBEL ASSET MANAGEMENT INC","Zurcher Kantonalbank (Zurich Cantonalbank)","Vontobel Holding Ltd.","Bank Julius Baer &amp; Co. Ltd, Zurich","Compagnie Lombard Odier SCmA","BANQUE PICTET &amp; CIE SA","UBS OCONNOR LLC","EDMOND DE ROTHSCHILD HOLDING S.A.","LGT CAPITAL PARTNERS LTD.","Lombard Odier Asset Management (USA) Corp","Banque Cantonale Vaudoise","Lombard Odier Asset Management (Europe) Ltd","Lombard Odier Asset Management (Switzerland) SA","Pictet North America Advisors SA","BANK PICTET &amp; CIE (ASIA) LTD","UBP Investment Advisors SA","Vontobel Swiss Wealth Advisors AG","PICTET BANK &amp; TRUST Ltd"],["000086117721000007","000113271621000004","000095012321001651","000156761921003445","000095012321002177","000108514621000832","000094787121000110","000158834021000003","000154459921000004","000142050621000143","000153560221000002","000113271621000005","000094787121000158","000106299321000400","000121465921001918","000179375521000002","000121465921001920","000121465921001917","000159957621000001","000159946921000001","000172758821000002","000108514621000388","000153563121000001"],[213426057089,3244733000,295785041,204405871,76622225,16923190,16421002,15257183,12182188,7773872,7356936,3903000,2979283,2887854,2084302,1805265,1656650,886957,788990,319758,194202,175776,159668]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>cik<\/th>\n      <th>company_name<\/th>\n      <th>filing_number<\/th>\n      <th>value_tot<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":4},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script>
```

Summen zusammenführen mit Angaben aus den `primary_doc`:


Wo stimmen die Beträge nicht überein?


```{=html}
<div id="htmlwidget-8e8e270dedba210599ab" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-8e8e270dedba210599ab">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9"],["861177","1132716","1610520","1588340","1615423","1615423","1132716","1649647","1641992"],["UBS ASSET MANAGEMENT AMERICAS INC","UBS OCONNOR LLC","UBS Group AG","Vontobel Holding Ltd.","Compagnie Lombard Odier SCmA","Compagnie Lombard Odier SCmA","UBS OCONNOR LLC","EDMOND DE ROTHSCHILD HOLDING S.A.","LGT CAPITAL PARTNERS LTD."],["000086117721000007","000113271621000004","000095012321001651","000158834021000003","000142050621000143","000142050621000143","000113271621000005","000094787121000158","000106299321000400"],[213426057089,3244733000,295785041,15257183,7773872,7773872,3903000,2979283,2887854],[213426057,3244733,295784923,15257184,3886936,3886936,3903,2979280,2887855]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>cik<\/th>\n      <th>company_name<\/th>\n      <th>filing_number<\/th>\n      <th>value_tot<\/th>\n      <th>table_value_total<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":[4,5]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script>
```

Gewisse stimmen nicht überein, da `value_tot` abgerundet wurden, andere wiederum weichen um mehr als den Faktor 980 ab — die Werte in den Filings unten wurden scheinbar nicht in Tausenden angegeben:


```{=html}
<div id="htmlwidget-13b378bc5f58dd431508" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-13b378bc5f58dd431508">{"x":{"filter":"none","data":[["1","2","3"],["861177","1132716","1132716"],["UBS ASSET MANAGEMENT AMERICAS INC","UBS OCONNOR LLC","UBS OCONNOR LLC"],["000086117721000007","000113271621000004","000113271621000005"],[213426057089,3244733000,3903000],[213426057,3244733,3903]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>cik<\/th>\n      <th>company_name<\/th>\n      <th>filing_number<\/th>\n      <th>value_tot<\/th>\n      <th>table_value_total<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":[4,5]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script>
```

Das wird im folgenden Schritt korrigiert: Die oberen Filings werden durch 1000 dividiert.
So sind alle Werte in der gleichen Grössenordnung.



Überprüfen ob das stimmt:

<img src="02-filings-first-look_files/figure-html/unnamed-chunk-12-1.png" width="672" />


## Beträge nach Investment Discretion

Die Banken schlüsseln die Portfoliopositionen nach der Art von Vermögensverwaltungsmandat, das sie innehaben (sog. Investment Discretion).


```{=html}
<div id="htmlwidget-7d0f69f4f157e60518a9" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-7d0f69f4f157e60518a9">{"x":{"filter":"none","data":[["1","2","3"],["DFND","SOLE","OTR"],[702855014.517,159594055.894,20891835.678],[79.5678100801306,18.067096722669,2.36509319720048]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>investment_discretion<\/th>\n      <th>value_tot<\/th>\n      <th>perc<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":[2,3]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script>
```


Nach Investment Discretion und Bank:
<img src="02-filings-first-look_files/figure-html/unnamed-chunk-14-1.png" width="672" />

Was bedeuten diese Werte? 

Das [Interpretive Statement No. 15292/November 2, 1978 der SEC auf S. 10](https://www.sec.gov/rules/interp/34-15292.pdf) gibt Aufschluss wann die Werte beispielsweise aussagen:

- `SOLE`
    - Die Bank/der institutionelle Investor entscheidet über jegliche Investments bzw. deren Strategie
    - Wenn Investment Discretion SOLE, dann gibt es nie einen Eintrag bei Other Manager. Bei Other Manager werden abgekürzt weitere Entitäten verzeichnet, die über Investment Discretion verfügen, voll aufgeführt werden diese im Primary Doc ([Bsp. Hier](https://www.sec.gov/Archives/edgar/data/1610520/000095012320011384/xslForm13F_X01/primary_doc.xml)).
    - «Reporting sole means that you do not control another reporting person, are not controlled by another reporting person, and do not otherwise share investment discretion with another institutional investment manager» (White Paper S. 17)
- `DFND` (shared-defined)
    - Investment Discretion wird geteilt mit Tochter- oder kontrollierendem Unternehmen
        - Eine Tochterfirma der Bank die die Aktien der SEC ausweist, verfügt über die Aktien/deren Strategie (gemäss SEC FAQ, Question 6 Q: What is "investment discretion"?).
    - «Shared-defined investment discretion is used when the reporting manager controls or is controlled by another legal entity.83For example, a bank holding company and its subsidiary would utilize the shared-defined category. Shared-defined is also used when investment discretion is shared between investment companies and investment advisers who advise those companies» [White Paper S. 17](http://assets.tabbforum.com/13F%20White%20Paper%20Final.pdf)
- `OTR`
    - «Finally, the shared-other category is used for any security in which investment discretion is shared in a manner other than the shared-defined category, such as multiple levels of shared investment discretion.86If investment discretion is shared, the reporting manager must identify the other institutional investment manager that shares investment discretion» ([White Paper S. 17](http://assets.tabbforum.com/13F%20White%20Paper%20Final.pdf))


## Inkludierte Offenlegungen für andere Manager und andere Manger, die für eine Bank Filings machen


### Included Filings:

In der folgenden Liste sehen wir, welche 13F-Offenlegungen für welche Töchter der Konzerne für den Zweck dieser Analyse zusammengelegt werden.

Die Struktur ist die folgende:

* Name der Bank (von mir gegeben)
   * Offenlegende Entität/Gesellschaft
      * Weitere inkludierte Gesellschaften/sog. Other Included Managers
      
Nachfolgend die Resultate:

* **Banque Cantonale Vaudoise**
    * Banque Cantonale Vaudoise (CIK: 1793755)
 * **Credit Suisse**
    * CREDIT SUISSE AG/ (CIK: 824468)
         * CREDIT SUISSE ASSET MANAGEMENT LLC/NY
         * CREDIT SUISSE INTERNATIONAL
         * MultiConcept Fund Management S.A.
         * Credit Suisse Fund Management S.A.
         * Credit Suisse Management LLC
         * CREDIT SUISSE HEDGING-GRIFFO SERVICOS INTERNACIONAIS S.A.
         * Credit Suisse Securities (Canada), Inc.
         * CREDIT SUISSE SECURITIES (EUROPE) LTD
         * Credit Suisse Funds AG
         * CREDIT SUISSE SECURITIES (USA) LLC
         * CREDIT SUISSE CAPITAL LLC/
         * Credit Suisse (Italy) S.p.a.
 * **Edmond de Rothschild**
    * EDMOND DE ROTHSCHILD HOLDING S.A. (CIK: 1649647)
         * EDMOND DE ROTHSCHILD (SUISSE) S.A.
         * Edmond de Rothschild (Europe)
         * EDMOND DE ROTHSCHILD ASSET MANAGEMENT (FRANCE)
         * EDMOND DE ROTHSCHILD ASSET MANAGEMENT (LUXEMBOURG)
         * EDMOND DE ROTHSCHILD (MONACO)
 * **Julius Bär**
    * Bank Julius Baer & Co. Ltd, Zurich (CIK: 1544599)
         * NSC Asesores
         * Kairos Partners
 * **LGT**
    * LGT CAPITAL PARTNERS LTD. (CIK: 1641992)
 * **Lombard Odier**
    * Compagnie Lombard Odier SCmA (CIK: 1615423)
         * Bank Lombard Odier & Co Ltd
         * Lombard Odier (Europe) S.A.
    * Lombard Odier Asset Management (Europe) Ltd (CIK: 1535784)
         * Compagnie Lombard Odier SCmA
    * Lombard Odier Asset Management (Switzerland) SA (CIK: 1535660)
         * Compagnie Lombard Odier SCmA
    * Lombard Odier Asset Management (USA) Corp (CIK: 1491719)
         * Compagnie Lombard Odier SCmA
 * **Pictet**
    * BANK PICTET & CIE (ASIA) LTD (CIK: 1599469)
    * BANQUE PICTET & CIE SA (CIK: 1535602)
    * PICTET ASSET MANAGEMENT LTD (CIK: 1291274)
         * Pictet Asset Management SA
    * PICTET BANK & TRUST Ltd (CIK: 1535631)
    * Pictet North America Advisors SA (CIK: 1599576)
 * **UBS**
    * UBS ASSET MANAGEMENT AMERICAS INC (CIK: 861177)
         * UBS GROUP AG
         * UBS ASSET MANAGEMENT TRUST COMPANY
         * UBS AG/UBS ASSET MANAGEMENT
         * UBS ASSET MANAGEMENT (UK) LTD
         * UBS ASSET MANAGEMENT LIFE LTD.
    * UBS Group AG (CIK: 1610520)
         * UBS Financial Services Inc.
         * UBS Swiss Financial Advisors AG
         * UBS AG New York Branch
    * UBS OCONNOR LLC (CIK: 1132716)
    * UBS OCONNOR LLC (CIK: 1132716)
 * **Union Bancaire Privee**
    * UBP Investment Advisors SA (CIK: 1727588)
 * **Vontobel**
    * VONTOBEL ASSET MANAGEMENT INC (CIK: 928633)
         * Vontobel Holding Ltd.
         * Virtus Investment Advisers, Inc.
    * Vontobel Holding Ltd. (CIK: 1588340)
         * Bank Vontobel Ltd
         * Vontobel Asset Management Ltd
         * Vontobel Asset Management S.A.
    * Vontobel Swiss Wealth Advisors AG (CIK: 1634222)
 * **ZKB**
    * Zurcher Kantonalbank (Zurich Cantonalbank) (CIK: 1368163)

Beim Betrachten der oberen Liste könnte man meinen, die UBS Americas würde auch Vermögenswerte der UBS Group gegenüber der SEC offenlegen. 

Gemäss UBS-Pressesprecher Plangg ist das nicht so (E-Mail vom 26. März):

> Habe gerade eine kurze Antwort erhalten. Die drei Filings sind separat und somit AM US & O’Connor nicht Teil des Group filings.


<!-- Für Analyse über Zeit: -->
<!-- Wer macht innerhalb einer Bank wann Filings? -->

<!-- ```{r} -->
<!-- library(zoo) -->
<!-- library(ggplot2) -->
<!-- filings %>% -->
<!--    mutate(filing_quarter = as.yearqtr(paste0(date_filed, "-", quarter))) %>%   -->
<!--    left_join(banken_nach_cik, by = c("cik")) %>%  -->
<!--    filter(investment_discretion == "SOLE") %>% -->
<!--    group_by(bank_name, company_name, filing_quarter) %>%  -->
<!--    summarize(sum_value_billion = sum(value/1e6, na.rm = TRUE)) %>%  -->
<!--    ggplot(aes(x = filing_quarter, y = sum_value_billion, color = company_name)) + -->
<!--    scale_x_yearqtr() + -->
<!--    scale_y_log10(labels = scales::label_dollar(big.mark = "'", suffix = " Mia.")) + -->
<!--    geom_line() + -->
<!--    geom_point() + -->
<!--    facet_wrap(bank_name~., scales = "free_y", ncol = 1) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- filings %>% -->
<!--    mutate(filing_quarter = as.yearqtr(paste0(date_filed, "-", quarter))) %>%   -->
<!--    left_join(banken_nach_cik, by = c("cik")) %>%  -->
<!--    # filter(investment_discretion == "OTR") %>%  -->
<!--    group_by(bank_name, company_name, filing_quarter) %>%  -->
<!--    summarize(sum_value_billion = sum(value/1e6, na.rm = TRUE)) %>%  -->
<!--    ggplot(aes(x = filing_quarter, y = sum_value_billion, group = company_name)) + -->
<!--    scale_x_yearqtr() + -->
<!--    scale_y_log10(labels = scales::label_dollar(big.mark = "'", suffix = " Mia.")) + -->
<!--    geom_line() + -->
<!--    geom_point() + -->
<!--    facet_wrap(bank_name~., scales = "free_y", ncol = 1) -->
<!-- ``` -->

<!-- ```{r} -->
<!-- filings %>% -->
<!--    mutate(filing_quarter = as.yearqtr(paste0(date_filed, "-", quarter))) %>%   -->
<!--    left_join(banken_nach_cik, by = c("cik")) %>%  -->
<!--    # filter(investment_discretion == "OTR") %>%  -->
<!--    group_by(bank_name, company_name, filing_quarter) %>%  -->
<!--    summarize(sum_value_billion = sum(value/1e6, na.rm = TRUE)) %>%  -->
<!--    ggplot(aes(x = filing_quarter, y = sum_value_billion, group = company_name)) + -->
<!--    scale_x_yearqtr() + -->
<!--    scale_y_log10(labels = scales::label_dollar(big.mark = "'", suffix = " Mia.")) + -->
<!--    geom_line() + -->
<!--    geom_point() + -->
<!--    facet_wrap(bank_name~., scales = "free_y", ncol = 1) -->
<!-- ``` -->


<!-- ```{r} -->
<!-- filings %>%  -->
<!--    left_join(banken_nach_cik, by = c("cik")) %>% -->
<!--    filter(bank_name == "UBS", date_filed == 2020, quarter == 4) %>% -->
<!--    group_by(company_name) %>%  -->
<!--    summarize(sum_value_billion = sum(value/1e6, na.rm = TRUE))  -->
<!-- ``` -->

<!-- ```{r} -->
<!-- filings %>%  -->
<!--    left_join(banken_nach_cik, by = c("cik")) %>% -->
<!--    filter(bank_name == "ZKB", date_filed == 2020, quarter == 4) %>% -->
<!--    group_by(company_name) %>%  -->
<!--    summarize(sum_value_billion = sum(value/1e6, na.rm = TRUE))  -->
<!-- ``` -->

<!-- ```{r} -->
<!-- filings %>% -->
<!--   left_join(banken_nach_cik, by = c("cik")) %>% -->
<!--   filter(bank_name == "ZKB", date_filed == 2020, quarter == 4) %>% -->
<!--   summarize(sum_value_billion = sum(value / 1e6, na.rm = TRUE)) -->
<!-- ``` -->

