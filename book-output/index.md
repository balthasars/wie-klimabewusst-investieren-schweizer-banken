---
title: "Wie klimabewusst legen Schweizer Grossbanken am US-Aktienmarkt an?"
date: "2022-04-17"
author: "Balthasar Sager"
site: bookdown::bookdown_site
output:
  bookdown::gitbook:
    df_print: paged
documentclass: book
biblio-style: apalike
link-citations: yes
gitlab-repo: balthasars/fossile-investitionen-geschaeftsbanken
description: "Diese Webseite dient dem einfacheren Teilen der Projektzwischenschritte sowie zur Dokumentation der Datenaufbereitung"
---

# Inhalt

Diese Webseite dient der Teiloffenlegung der Analyse (ausgeschlossen die Daten von Refinitiv).

Konkrete Zahlen zu den fünf untersuchten Banken finden Sie [hier](#textnumbers). 

Die veröffentlichte Analyse in der Republik enthält nur Zahlen für Credit Suisse, Lombard Odier, Pictet, UBS und Zürcher Kantonalbank.
Die Zahlen der restlichen Banken sind mit _äusserster_ Vorsicht zu geniessen — z.B. bei Vontobel gibt es Anzeichen, dass Tochterfirmen fehlen.

